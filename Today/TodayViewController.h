//
//  TodayViewController.h
//  Today
//
//  Created by Eric Betts on 11/24/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreLocation/CoreLocation.h>
#import "WeatherReport.h"

@interface TodayViewController : NSViewController <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    CLLocation *location;
}

@end
