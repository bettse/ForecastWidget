//
//  WeatherReport.h
//  ForecastWidget
//
//  Created by Eric Betts on 11/24/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

@interface WeatherReport : NSObject {
    NSString *desc;
    NSString *timeframe;
    NSString *iconName;
}

- (id)initWithTimeframe:(NSString *)aTimeframe
           andIcon:(NSString *)anIcon
           andDesc:(NSString *)aDesc;

@end
