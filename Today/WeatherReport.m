//
//  WeatherReport.m
//  ForecastWidget
//
//  Created by Eric Betts on 11/24/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import "WeatherReport.h"

@implementation WeatherReport


- (id)initWithTimeframe:(NSString *)aTimeframe
           andIcon:(NSString *)anIcon
           andDesc:(NSString *)aDesc
{
    if( self = [super init] ) {
        timeframe = aTimeframe;
        iconName = anIcon;
        desc = aDesc;
    }
    return self;
}


-(NSImage*) icon {
    NSString *filename = [NSString stringWithFormat:@"%@.png", iconName];
    NSImage *icon = [NSImage imageNamed:filename];
    NSLog(@"%s: %@ %i", __PRETTY_FUNCTION__, filename, icon.valid);
    if (icon.valid) {
        return icon;
    }

    return nil;
}

@end
