//
//  ListRowViewController.m
//  Today
//
//  Created by Eric Betts on 11/24/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import "ListRowViewController.h"

@implementation ListRowViewController

- (NSString *)nibName {
    return @"ListRowViewController";
}

- (void)loadView {
    [super loadView];

    // Insert code here to customize the view
}

@end
