//
//  TodayViewController.m
//  Today
//
//  Created by Eric Betts on 11/24/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import "TodayViewController.h"
#import "ListRowViewController.h"
#import <NotificationCenter/NotificationCenter.h>

@interface TodayViewController () <NCWidgetProviding, NCWidgetListViewDelegate, NCWidgetSearchViewDelegate>

@property (strong) IBOutlet NCWidgetListViewController *listViewController;
@property (strong) NCWidgetSearchViewController *searchController;

@end


@implementation TodayViewController

#pragma mark - NSViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Turn on CoreLocation
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
    // Set up the widget list view controller.
    // The contents property should contain an object for each row in the list.
    self.listViewController.contents = self.requestWeather;
}

- (void)dismissViewController:(NSViewController *)viewController {
    [super dismissViewController:viewController];

    // The search controller has been dismissed and is no longer needed.
    if (viewController == self.searchController) {
        self.searchController = nil;
    }
}

#pragma mark - NCWidgetProviding

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult result))completionHandler {
    // Refresh the widget's contents in preparation for a snapshot.
    // Call the completion handler block after the widget's contents have been
    // refreshed. Pass NCUpdateResultNoData to indicate that nothing has changed
    // or NCUpdateResultNewData to indicate that there is new data since the
    // last invocation of this method.
    completionHandler(NCUpdateResultNoData);
}

- (NSEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(NSEdgeInsets)defaultMarginInset {
    // Override the left margin so that the list view is flush with the edge.
    defaultMarginInset.left = 0;
    return defaultMarginInset;
}

- (BOOL)widgetAllowsEditing {
    // Return YES to indicate that the widget supports editing of content and
    // that the list view should be allowed to enter an edit mode.
    return NO;
}

- (void)widgetDidBeginEditing {
    // The user has clicked the edit button.
    // Put the list view into editing mode.
    self.listViewController.editing = YES;
}

- (void)widgetDidEndEditing {
    // The user has clicked the Done button, begun editing another widget,
    // or the Notification Center has been closed.
    // Take the list view out of editing mode.
    self.listViewController.editing = NO;
}

#pragma mark - NCWidgetListViewDelegate

- (NSViewController *)widgetList:(NCWidgetListViewController *)list viewControllerForRow:(NSUInteger)row {
    // Return a new view controller subclass for displaying an item of widget
    // content. The NCWidgetListViewController will set the representedObject
    // of this view controller to one of the objects in its contents array.
    return [[ListRowViewController alloc] init];
}

- (void)widgetListPerformAddAction:(NCWidgetListViewController *)list {
    // The user has clicked the add button in the list view.
    // Display a search controller for adding new content to the widget.
    self.searchController = [[NCWidgetSearchViewController alloc] init];
    self.searchController.delegate = self;

    // Present the search view controller with an animation.
    // Implement dismissViewController to observe when the view controller
    // has been dismissed and is no longer needed.
    [self presentViewControllerInWidget:self.searchController];
}

- (BOOL)widgetList:(NCWidgetListViewController *)list shouldReorderRow:(NSUInteger)row {
    // Return YES to allow the item to be reordered in the list by the user.
    return NO;
}

- (void)widgetList:(NCWidgetListViewController *)list didReorderRow:(NSUInteger)row toRow:(NSUInteger)newIndex {
    // The user has reordered an item in the list.
}

- (BOOL)widgetList:(NCWidgetListViewController *)list shouldRemoveRow:(NSUInteger)row {
    // Return YES to allow the item to be removed from the list by the user.
    return NO;
}

- (void)widgetList:(NCWidgetListViewController *)list didRemoveRow:(NSUInteger)row {
    // The user has removed an item from the list.
}

#pragma mark - NCWidgetSearchViewDelegate

- (void)widgetSearch:(NCWidgetSearchViewController *)searchController searchForTerm:(NSString *)searchTerm maxResults:(NSUInteger)max {
    // The user has entered a search term. Set the controller's searchResults property to the matching items.
    searchController.searchResults = @[];
}

- (void)widgetSearchTermCleared:(NCWidgetSearchViewController *)searchController {
    // The user has cleared the search field. Remove the search results.
    searchController.searchResults = nil;
}

- (void)widgetSearch:(NCWidgetSearchViewController *)searchController resultSelected:(id)object {
    // The user has selected a search result from the list.
}

#pragma mark - Weather Code


-(NSArray*) requestWeather {
    NSArray *reports = @[];
    
    NSString *apikey = @"ec3dca9f9390c21b168dd5ff08a8b294";
    NSString *urlFormat = @"https://api.forecast.io/forecast/%@/%@,%@";
    NSURLResponse *resp = nil;

    NSError *error = nil;
    NSString* lat = [NSString stringWithFormat:@"%3.4f", location.coordinate.latitude];
    NSString* lon = [NSString stringWithFormat:@"%3.4f", location.coordinate.longitude];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:urlFormat, apikey, lat, lon]];
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:url];
    NSData *response = [NSURLConnection sendSynchronousRequest: theRequest returningResponse: &resp error: &error];
    id jsonData = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        return reports;
    }
    
    NSString *nowIcon = jsonData[@"currently"][@"icon"];
    NSString *nowSummary = jsonData[@"currently"][@"summary"];
    NSString *nowTemp = jsonData[@"currently"][@"temperature"];
    NSString *nowDesc = [NSString stringWithFormat:@"%@°\n%@", nowTemp, nowSummary];
    WeatherReport *now = [[WeatherReport alloc] initWithTimeframe:@"Right now" andIcon:nowIcon andDesc:nowDesc];
    
    NSString *nextIcon = jsonData[@"minutely"][@"icon"];
    NSString *nextDesc = jsonData[@"minutely"][@"summary"];
    WeatherReport *next = [[WeatherReport alloc] initWithTimeframe:@"Next" andIcon:nextIcon andDesc:nextDesc];
    
    NSString *laterIcon = jsonData[@"hourly"][@"icon"];
    NSString *laterDesc = jsonData[@"hourly"][@"summary"];
    WeatherReport *later = [[WeatherReport alloc] initWithTimeframe:@"Later" andIcon:laterIcon andDesc:laterDesc];
    
    return @[now, next, later];
}

#pragma mark - Location Code

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    // Ignore updates where nothing we care about changed
    if (newLocation.coordinate.longitude == oldLocation.coordinate.longitude &&
        newLocation.coordinate.latitude == oldLocation.coordinate.latitude &&
        newLocation.horizontalAccuracy == oldLocation.horizontalAccuracy)
    {
        return;
    }
    NSLog(@"Location update: %@", newLocation);
    
    location = newLocation;
}
@end
