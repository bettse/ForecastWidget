//
//  AppDelegate.h
//  ForecastWidget
//
//  Created by Eric Betts on 11/24/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

