//
//  main.m
//  ForecastWidget
//
//  Created by Eric Betts on 11/24/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
